angular.module('angularDemoApp').factory('MenuService',
	function ($state, $rootScope, $http, $window) {

		var version = {};

		var sections = [{
			name: 'menu.static.DASHBOARD',
			link: 'app.dashboard',
			icon: 'fa-dashboard',
			type: 'link'
		}];


				sections.push({
					link: 'app.conference.list',
					icon: 'glyphicon-cloud',
					name: 'menu.domain.conference',
					type: 'link'
				});

				
				sections.push({
					link: 'app.conferenceRoom.list',
					icon: 'glyphicon-envelope',
					name: 'menu.domain.conferenceRoom',
					type: 'link'
				});

				
				sections.push({
					link: 'app.participant.list',
					icon: 'glyphicon-glass',
					name: 'menu.domain.participant',
					type: 'link'
				});

				
		

		sections.push();

		function sortByName(a, b) {
			return a.name < b.name ? -1 : 1;
		}

		var self = {
			version: version,
			sections: sections,

			selectSection: function (section) {
				self.openedSection = section;
			},
			toggleSelectSection: function (section) {
				self.openedSection = (self.openedSection === section ? null : section);
			},
			isSectionSelected: function (section) {
				return self.openedSection === section;
			},

			selectPage: function (section, page) {
				self.currentSection = section;
				self.currentPage = page;
			},
			isPageSelected: function (page) {
				return self.currentPage === page;
			}
		};

		 $rootScope.$on('$stateChangeSuccess', onLocationChange);
		onLocationChange(null, $state.$current);

		return self;

		function onLocationChange(event, toState, toParams, fromState, fromParams) {

			var path = toState.name;
			var settingsLink = {
				name: 'pages.settings.view.title',
				link: 'app.settings',
				type: 'link'
			};

			if (path == settingsLink.link) {
				self.selectSection(settingsLink);
				self.selectPage(settingsLink, settingsLink);
				return;
			}

			var matchPage = function (section, page) {
				if (path === page.link) {
					self.selectSection(section);
					self.selectPage(section, page);
				}
			};

			sections.forEach(function (section) {
				if (section.children) {
					// matches nested section toggles, such as API or Customization
					section.children.forEach(function (childSection) {
						if (childSection.pages) {
							childSection.pages.forEach(function (page) {
								matchPage(childSection, page);
							});
						}
					});
				}
				else if (section.pages) {
					// matches top-level section toggles, such as Demos
					section.pages.forEach(function (page) {
						matchPage(section, page);
					});
				}
				else if (section.type === 'link') {
					// matches top-level links, such as "Getting Started"
					matchPage(section, section);
				}
			});
		}
	});