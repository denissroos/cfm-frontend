{
"app":{
	"name":"Konverentside halduse rakendus",
	"title":"Konverentside halduse rakendus"
},
"button": {
	"submit": "Save",
	"back": "Back",
	"cancel": "Cancel",
	"delete":"Delete",
	"edit":"Edit",
	"copy": "Copy",
	"search":"Search",
	"true":"True",
	"false":"False",
	"none":"None"
},
"searchSelect":{
	"true":"True",
	"false":"False",
	"none":""
},
"tabs":{
	"selecttab": "Click on tab to show table"
},
"header" : {
  "navbar" : {
    "UPLOAD" : "Upload",
    "new" : {
      "NEW" : "New"
    },
    "user":{
    	"settings":"Settings",
    	"docs":"Help"
    },
    "NOTIFICATIONS" : "Notifications",
	"logout":"Logout"
  }
},
"messages":{
	"loading": "Loading ..."
},
"upload":{
	"title":"Upload files",
	"file":"Upload file",
	"or":"OR",
	"drop":"Drag And Drop your file here",
	"queue":"Upload queue",
	"length":"Queue lenght",
	"progress":"Queue progress",
	"uploadAll":"Upload all",
	"cancelAll":"Cancel all",
	"removeAll":"Remove all",
	"upload":"Upload",
	"cancel":"Cancel",
	"remove":"Remove",
	"name":"Name",
	"size":"Size",
	"progress":"Progress",
	"status":"Status",
	"actions":"Actions"
},
"pages":{
	"Dashboard":{
		"title":"Welcome",
		"sub-title-1":"Statistics",
		"conferences":"Conferences",
		"conferencerooms":"Conference rooms",
		"participants":"Participants"
	},
	"error":{
		"title": "Error Page"
	},
	"session":{
		"messages":{
			"default": "There was error loading page",
			"permission-denied": "You don't have permissions to view page: ",
			"relogin":"Not authorized to view page",
			"forbidden":"Forbidden to view page",
			"logging-out": "Logging out ...",
			"logged-out": "Logged out",
			"already-logged-out": "Already logged out",
			"state-change-error": "State change error",
			"conference-room-capacity-limit-exceeds":"Conference room capacity limit exceeds"

		},
		"logout":{
			"login-again":"Login again",
			"logout-cas":"Logout from CAS"
		}
	},
	"settings":{
		"title":"Settings",
		"view":{
			"title":"Settings",
			"info":{
				"title":"User info",
				"permissions":"Permissions"
			}
		}
	},

	
		"conference":{
			"name":"Conference",
			"defaultFieldName":{
			
						"conferenceRoom":"Conference Room",					
						"dateCreatead":"Date Created",
						"description":"Description",					
						"lastUpdated":"Last Updated",					
						"name":"Name",					
						"startTime":"Start Time",					
				"id": "Id"
			},

			"list":{
				"title": "Conference List",	
				"new": " New Conference",
				"table":{
					"title":"Conference Table"
				}
			},
			"view":{
				"title": "Conference View",
				"lists": "{{isval}} in {{inval}}",
				"edit":{
					"title": "Edit Conference",
						"form":{
						"title":"Conference Form"
					}
				}
			},

			"messages":{
				"delete": "Deleted Conference",
				"update": "Updated Conference",
				"create": "Created Conference"
			},	
			"create":{
				"title": "Create Conference"
			}
		},
	
		"conferenceRoom":{
			"name":"Conference Room",
			"defaultFieldName":{
			
						"capacity":"Capacity",					
						"dateCreated":"Date Created",					
						"description":"Description",					
						"lastUpdated":"Last Updated",					
						"location":"Location",					
						"name":"Name",					
				"id": "Id"
			},

			"list":{
				"title": "Conference Room List",	
				"new": " New Conference Room",
				"table":{
					"title":"Conference Room Table"
				}
			},
			"view":{
				"title": "Conference Room View",
				"lists": "{{isval}} in {{inval}}",
				"edit":{
					"title": "Edit Conference Room",
						"form":{
						"title":"Conference Room Form"
					}
				}
			},

			"messages":{
				"delete": "Deleted Conference Room",
				"update": "Updated Conference Room",
				"create": "Created Conference Room"
			},	
			"create":{
				"title": "Create Conference Room"
			}
		},
	
		"participant":{
			"name":"Participant",
			"defaultFieldName":{
			
						"birthdate":"Birthdate",					
						"conference":"Conference",					
						"dateCreated":"Date Created",					
						"email":"Email",					
						"firstname":"Firstname",					
						"lastUpdated":"Last Updated",					
						"lastname":"Lastname",					
				"id": "Id"
			},

			"list":{
				"title": "Participant List",	
				"new": " New Participant",
				"table":{
					"title":"Participant Table"
				}
			},
			"view":{
				"title": "Participant View",
				"lists": "{{isval}} in {{inval}}",
				"edit":{
					"title": "Edit Participant",
						"form":{
						"title":"Participant Form"
					}
				}
			},

			"messages":{
				"delete": "Deleted Participant",
				"update": "Updated Participant",
				"create": "Created Participant"
			},	
			"create":{
				"title": "Create Participant"
			}
		}
	
},
"menu" :{
	"static":{
		"HEADER" : "Navigation",
		"DASHBOARD" : "Dashboard"  
	},
	"domain":{
	
		"conference":"Conference",
	
		"conferenceRoom":"Conference Room",
	
		"participant":"Participant"
	
	},
	"package":{
	
		"cfm":"Cfm"
	
	}
  }
}

