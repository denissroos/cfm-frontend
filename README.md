Konverentside halduse rakendus spetsifikatsioon
===========================================
Fron-end rakendus - AngularJS

Tegemist on rakendusega, millega saab hallata konverentse. 
Funktsionaalsus (baasfunktsionaalsus):

* kasutaja loob konverentsi;
* kasutaja tühistab konverentsi;
* kasutaja kontrollib konverentsiruumide saadavust (vastavalt registreerunud kasutajatele)
* kasutaja lisab osalise konverentsi;
* kasutaja eemaldab osalise konverentsist.


Paigaldusjuhend
===========================================
 
## Rakenduse konfiguratsioon

config.json
```
 "restUrl": "http://localhost:8080/cfm-backend"
```
##  Paigaldamine

Kopeeri sisu oma veebiserverisse




