'use strict';

angular.module('angularDemoApp').provider('SessionService', function ($authProvider, appConfig) {

	$authProvider.loginRoute = '/login';
	$authProvider.tokenName = 'access_token';
	$authProvider.authHeader = 'Authorization';
	$authProvider.defaultUrlParams = ['service'];
	$authProvider.logoutRedirect = null;

	console.log('Configuring CAS as Oauth');

	$authProvider.oauth2({
		name: 'cas',
		url: appConfig.restUrl + '/login',
		redirectUri: 'app/',
		clientId: '',
		responseParams: ['ticket'],
		requiredUrlParams: ['service'],
		defaultUrlParams: [],
		service: appConfig.redirectUrl,
		authorizationEndpoint: appConfig.loginUrl
	});

	this.$get = function ($localStorage, $auth, $q, appConfig, $http, $rootScope, $mdDialog) {
		var service = {};
		var _currentUser = {};

		var _loginModal = null;

		$rootScope.$on('show-relogin-modal', function () {
			if (!_loginModal) {
				showTheModal();
			}
		});

		function showTheModal() {
			_loginModal = $mdDialog.show({
				templateUrl: 'app/logout/logout.html',
				controller: 'ReloginController'
			});

			_loginModal.finally(function () {
				console.log("Nulling _loginModal")
				_loginModal = null;
			});
		}

		service.login = function () {
			console.log('Starting to login to CAS');

			var deferred = $q.defer();

			$auth.link('cas', {serviceUrl: appConfig.redirectUrl}).then(function (response) {
				console.log('Logged in.');
				// save settings to local storage
				$localStorage.userData = _currentUser = response.data;
				if (_loginModal) {
					$mdDialog.hide();
				}

				deferred.resolve(response.data);
			}).catch(function (response) {
				console.log('Error when login to CAS');
				deferred.reject(response);
			});

			return deferred.promise;
		};

		service.fetchUserData = function (username) {
			var deferred = $q.defer();
			var userInfo = {};

			//Query additional data and then resolve
			deferred.resolve(userInfo);
			AutocompleteService.amrAmetnikQuery(username).then(function (ametnik) {
				ametnikInfo.ametnik = ametnik;
				ametnikInfo.ametnik.nimi = ametnikInfo.ametnik.eesnimi + ' ' + ametnikInfo.ametnik.perekonnanimi;
				AutocompleteService.amrYksusQuery(ametnik.yksusId).then(function (yksus) {
					ametnikInfo.yksus = yksus;
					deferred.resolve(userInfo);
				});
			});
			return deferred.promise;
		}


		service.logout = function () {
			delete $localStorage.userData;
			_currentUser = {};
			return $http.post(appConfig.logoutUrl).then(function () {
				$auth.logout();
			});
		};

		service.getCurrentUser = function () {
			var deferred = $q.defer();
			if (!_.isEmpty(_currentUser)) {
				if (_.isEmpty(_currentUser.ametnik)) {
					service.fetchUserData(_currentUser.login).then(function (data) {
						angular.extend(_currentUser, data);
						$localStorage.userData = _currentUser;
						deferred.resolve(_currentUser);
					});
				} else {
					deferred.resolve(_currentUser);
				}
			} else if (angular.isDefined($localStorage.userData)) {
				_currentUser = $localStorage.userData;
				deferred.resolve(_currentUser);
			}

			return deferred.promise;
		};

		service.hasRole = function (role) {
			if (_.isEmpty(_currentUser) || _.isEmpty(_currentUser.permissions)) {
				return false;
			}

			return _.indexOf(_currentUser.permissions, role) !== -1;
		};

		service.isAuthenticated = function () {
			return !_.isEmpty(service.getCurrentUser());
		};

		return service;
	};
});

