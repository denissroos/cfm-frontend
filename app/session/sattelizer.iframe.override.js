'use strict';

angular.module('satellizer')
  .factory('satellizer.iframeModal', [
    '$q',
    '$interval',
    '$window',
    '$location',
    'satellizer.utils',
    '$mdDialog',
    'satellizer.config',
    function($q, $interval, $window, $location, utils, $mdDialog, config) {
      var polling = null;

      var popup = {};

      popup.open = function(url, options) {

        if(angular.element("#cas_iframe").length==0){
          var popupWindow = $mdDialog.show({
            template: '<md-dialog aria-label="login">'+
                      '<md-dialog-content>'+
                      '<iframe id="cas_iframe" ng-src="'+url+'" frameborder="0" height="650" width="600"></iframe>'+
                      '</md-dialog-content>'+
                      '</md-dialog>'
          });

          popupWindow.then(function () {
            console.log("Closed CAS window");
            $interval.cancel(polling);
          }, function () {
            console.log("Closed CAS window");
            $interval.cancel(polling);
          });

        }else{
          console.error("There is already an iframe 'cas_iframe' on page");
          return null;
        }

        return popup.pollPopup();
      };

      popup.pollPopup = function() {
        var deferred = $q.defer();
        polling = $interval(function() {
          try {
            var iframe = angular.element("#cas_iframe")[0];
            if (iframe.src.indexOf(document.domain) !==-1  && (iframe.contentDocument.location.search || iframe.contentDocument.location.hash)) {
              var queryParams = iframe.contentDocument.location.search.substring(1).replace(/\/$/, '');
              var hashParams = iframe.contentDocument.location.hash.substring(1).replace(/\/$/, '');
              var hash = utils.parseQueryString(hashParams);
              var qs = utils.parseQueryString(queryParams);

              angular.extend(qs, hash);

              if (qs.error) {
                deferred.reject({ error: qs.error });
              } else {
                deferred.resolve(qs);
              }
              $mdDialog.hide();
              $interval.cancel(polling);
            }
          } catch (error) {
            //console.log(error)
          }

        }, 35);
        return deferred.promise;
      };

      popup.prepareOptions = function(options) {
        var width = options.width || 500;
        var height = options.height || 500;
        return angular.extend({
          width: width,
          height: height,
          left: $window.screenX + (($window.outerWidth - width) / 2),
          top: $window.screenY + (($window.outerHeight - height) / 2.5)
        }, options);
      };

      popup.stringifyOptions = function(options) {
        var parts = [];
        angular.forEach(options, function(value, key) {
          parts.push(key + '=' + value);
        });
        return parts.join(',');
      };

      return popup;
    }])
  .factory('satellizer.Oauth2', [
    '$q',
    '$http',
    '$window',
    'satellizer.iframeModal',
    'satellizer.utils',
    'satellizer.config',
    function($q, $http, $window, popup, utils, config) {
      return function() {

        var defaults = {
          url: null,
          name: null,
          state: null,
          scope: null,
          scopeDelimiter: null,
          clientId: null,
          redirectUri: null,
          popupOptions: null,
          authorizationEndpoint: null,
          responseParams: null,
          requiredUrlParams: null,
          optionalUrlParams: null,
          defaultUrlParams: ['response_type', 'client_id', 'redirect_uri'],
          responseType: 'code'
        };

        var oauth2 = {};

        oauth2.open = function(options, userData) {
          angular.extend(defaults, options);

          var stateName = defaults.name + '_state';

          if (angular.isFunction(defaults.state)) {
            $window.localStorage[stateName] = defaults.state();
          } else if(angular.isString(defaults.state)) {
            $window.localStorage[stateName] = defaults.state;
          }

          var url = defaults.authorizationEndpoint + '?' + oauth2.buildQueryString();

          return popup.open(url, defaults.popupOptions)
            .then(function(oauthData) {
              if (defaults.responseType === 'token') {
                return oauthData;
              }
              if (oauthData.state && oauthData.state !== $window.localStorage[stateName]) {
                return $q.reject('OAuth 2.0 state parameter mismatch.');
              }

              return oauth2.exchangeForToken(oauthData, userData);
            });

        };

        oauth2.exchangeForToken = function(oauthData, userData) {
          var data = angular.extend({}, userData, {
            code: oauthData.code,
            clientId: defaults.clientId,
            redirectUri: defaults.redirectUri
          });

          if (oauthData.state) {
            data.state = oauthData.state;
          }

          angular.forEach(defaults.responseParams, function(param) {
            data[param] = oauthData[param];
          });

          return $http.post(defaults.url, data, { withCredentials : config.withCredentials });
        };

        oauth2.buildQueryString = function() {
          var keyValuePairs = [];
          var urlParams = ['defaultUrlParams', 'requiredUrlParams', 'optionalUrlParams'];

          angular.forEach(urlParams, function(params) {
            angular.forEach(defaults[params], function(paramName) {
              var camelizedName = utils.camelCase(paramName);
              var paramValue = defaults[camelizedName];

              if (paramName === 'state') {
                var stateName = defaults.name + '_state';
                paramValue = $window.localStorage[stateName];
              }

              if (paramName === 'scope' && Array.isArray(paramValue)) {
                paramValue = paramValue.join(defaults.scopeDelimiter);

                if (defaults.scopePrefix) {
                  paramValue = [defaults.scopePrefix, paramValue].join(defaults.scopeDelimiter);
                }
              }

              keyValuePairs.push([paramName, paramValue]);
            });
          });

          return keyValuePairs.map(function(pair) {
            return pair.join('=');
          }).join('&');
        };

        return oauth2;
      };
    }]);
