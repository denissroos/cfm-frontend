'use strict';

angular.module('angularDemoApp')
  .controller('SettingsController', function ($http,$scope,appConfig) {
  $scope.statistics = {};
    $http.get(appConfig.restUrl +'/dashboard/v1')
        .then(
            function(response){
              // success callback
              $scope.statistics=response
            },
            function(response){
              // failure call back
            }
        );
  });
