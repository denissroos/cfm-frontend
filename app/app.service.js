'use strict';
/**
 * Generated autocomplete services to quickly demo autocomplete functionality
 */
angular.module('angularDemoApp')
  .factory('AutocompleteService', function($resource, appConfig){
  	var toLabel = function(model, labelProperties){
  		if(model){
  			var str = '';
			angular.forEach(labelProperties, function(label, i) {
				if(i > 0){
					str += ' ';
				}
				str += model[label];
			}, str);
			return str;
		}
	    return '';
  	};
  	var toAutocompleteObject = function(item, labelProperties, tagsOutput){
  		var obj = {id:item.id};
  		if(tagsOutput){
  			obj.name = _.map(labelProperties, function(label) { return item[label];  }).join(', ');
	    }else if(labelProperties){
		    angular.forEach(labelProperties, function(label) {
			    obj[label] = item[label];
		    }, item);
	    }else{
		    obj.name = autocompleteObjToString(item);
	    }
	    return obj;
    };
  	
  	var resourceQuery = function(val, urlPart, labelProperties, excludes, tagsOutput){
  		var param = {max: 1500};
		param.searchString = val;
		param.excludes = excludes;
		var resource = $resource(appConfig.restUrl + '/' + urlPart);
		return resource.query(param).$promise.then(
	        function( response ){
		       	return response.map(function(item){
		       		return toAutocompleteObject(item, labelProperties, tagsOutput);
			    });
	       	}
     	);
  	};

	var autocompleteObjToString = function(model){
		var str = '';
		var stringify = function(obj){
			_.forIn(obj, function(value) {
				if(_.isObject(value)){
					stringify(value);
				} else {
					if(str !== ''){
						str += ' ';
					}
					str += value;
				}
			});
		};
		stringify(model);
		return str;
	};

  	var service = {
  		promiseToLabel:function(model, labelProperties){
			model.$promise.then(function() {
				model.name = toLabel(model, labelProperties);
			});
			return model;
		},
	

	
  		conferenceQuery : function(val, labelProperties, tagsOutput){
  			return resourceQuery(val, 'conferences/v1', labelProperties, 'conferenceRoom', tagsOutput);
	    },
	    conferenceFormatLabel : function(model, labelProperties) {
		    return toLabel(model, labelProperties);
		},
    
  		conferenceRoomQuery : function(val, labelProperties, tagsOutput){
  			return resourceQuery(val, 'conferencerooms/v1', labelProperties, '', tagsOutput);
	    },
	    conferenceRoomFormatLabel : function(model, labelProperties) {
		    return toLabel(model, labelProperties);
		},
    
  		participantQuery : function(val, labelProperties, tagsOutput){
  			return resourceQuery(val, 'participants/v1', labelProperties, 'conference', tagsOutput);
	    },
	    participantFormatLabel : function(model, labelProperties) {
		    return toLabel(model, labelProperties);
		},
    
    };
    return service;
  });
