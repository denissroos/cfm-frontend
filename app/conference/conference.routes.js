'use strict';

angular.module('angularDemoApp')
	.config(function ($stateProvider) {
$stateProvider
		.state('app.conference', {
		    url: '/conference',
			abstract: true,
		    template: '<div ui-view="page" class="fade-in-up"></div>'
		})
		.state('app.conference.list', {
			url: '/list?search',//TODO: search so that search is not an object in url
			views: {
				"page@app.conference": {
					templateUrl: 'app/conference/conference.list.html',
					controller: 'ConferenceListController'
				}
			}
		}).state('app.conference.create',{
			url: '/create',
			ncyBreadcrumb: {
				parent: 'app.conference.list'
			},
			views: {
				"page@app.conference": {
					templateUrl: 'app/conference/conference.form.html',
					controller: 'ConferenceEditController'
				}
			},
			resolve:{
				conferenceData: function($stateParams, ConferenceService) {
					return new ConferenceService();
				}
			}
		}).state('app.conference.view',{
			url: '/view/:id',
			ncyBreadcrumb: {
				parent: 'app.conference.list'
			},
			views: {
				"page@app.conference": {
					templateUrl: 'app/conference/conference.view.html',
					controller: 'ConferenceViewController'
				}
			},
			resolve:{
				conferenceData: function($stateParams, ConferenceService){
					return ConferenceService.get({id:$stateParams.id}).$promise.then(
						function( response ){
							return response;
						}
					);
				}
			}
		}).state('app.conference.view.edit',{
			url: '/edit',
			views: {
				"page@app.conference": {
					templateUrl: 'app/conference/conference.form.html',
					controller: 'ConferenceEditController',
				}
			},
			resolve:{
				conferenceData: function($stateParams, ConferenceService){
					return ConferenceService.get({id:$stateParams.id}).$promise.then(
						function( response ){
								return response;
						}
					);
				}
			}
		})

	.state('app.conference.view.conferenceRoomModal',{
		url: '/modal/conferenceRoom/:modalId',
		data:{
			isModal:true
		},
		onEnter: function($stateParams, $state, $mdDialog, $resource) {
			var modalId = $stateParams.modalId;
			$mdDialog.show({
				templateUrl: 'app/conferenceRoom/conferenceRoom.view.modal.html',
				resolve: {
					conferenceRoomData: function($stateParams, ConferenceRoomService){
						//TODO: Add parent ($stateParams.id) to query
						return ConferenceRoomService.get({id:modalId}).$promise.then(
							function( response ){
								return response;
							}
						);
					}
				},
				controller: 'ConferenceRoomViewController',

			}).then(function () {
				$state.go('^');
			});
		}

	}).state('app.conference.view.edit.conferenceRoomSearchModal',{
		templateUrl: 'app/conferenceRoom/conferenceRoom.list.modal.html',
		controller: 'ConferenceRoomListController'
	})
	

		.state('app.conference.view.participant',{
			url: '/participant/:relationName',
			ncyBreadcrumb: {
				skip: true
			},
			data:{
				isTab:true
			},
			views: {
				"tabs": {
					templateUrl: 'app/participant/participant.list.html',
					controller: 'ParticipantListController'
				}
			}
		})
	
;
});