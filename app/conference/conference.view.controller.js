'use strict';

angular.module('angularDemoApp')
    .controller('ConferenceViewController', function ($scope, $state, $stateParams, $translate, inform,
            ConferenceService, conferenceData,$mdDialog) {
	 	$scope.conference = conferenceData;

		if($state.current.data){
			$scope.isModal = $state.current.data.isModal;
		}

		$scope.deleteConference = function(instance){
			return ConferenceService.deleteInstance(instance).then(function(instance){
				$state.go('^.list');
				return instance;
			});
		};
		$scope.go = function(route){
			$state.go(route);
		};
		$scope.closeItemViewer = function () {
			$mdDialog.hide();
		};
	});
