'use strict';

angular.module('angularDemoApp')
	.config(function ($stateProvider) {
$stateProvider
		.state('app.conferenceRoom', {
		    url: '/conferenceRoom',
			abstract: true,
		    template: '<div ui-view="page" class="fade-in-up"></div>'
		})
		.state('app.conferenceRoom.list', {
			url: '/list?search',//TODO: search so that search is not an object in url
			views: {
				"page@app.conferenceRoom": {
					templateUrl: 'app/conferenceRoom/conferenceRoom.list.html',
					controller: 'ConferenceRoomListController'
				}
			}
		}).state('app.conferenceRoom.create',{
			url: '/create',
			ncyBreadcrumb: {
				parent: 'app.conferenceRoom.list'
			},
			views: {
				"page@app.conferenceRoom": {
					templateUrl: 'app/conferenceRoom/conferenceRoom.form.html',
					controller: 'ConferenceRoomEditController'
				}
			},
			resolve:{
				conferenceRoomData: function($stateParams, ConferenceRoomService) {
					return new ConferenceRoomService();
				}
			}
		}).state('app.conferenceRoom.view',{
			url: '/view/:id',
			ncyBreadcrumb: {
				parent: 'app.conferenceRoom.list'
			},
			views: {
				"page@app.conferenceRoom": {
					templateUrl: 'app/conferenceRoom/conferenceRoom.view.html',
					controller: 'ConferenceRoomViewController'
				}
			},
			resolve:{
				conferenceRoomData: function($stateParams, ConferenceRoomService){
					return ConferenceRoomService.get({id:$stateParams.id}).$promise.then(
						function( response ){
							return response;
						}
					);
				}
			}
		}).state('app.conferenceRoom.view.edit',{
			url: '/edit',
			views: {
				"page@app.conferenceRoom": {
					templateUrl: 'app/conferenceRoom/conferenceRoom.form.html',
					controller: 'ConferenceRoomEditController',
				}
			},
			resolve:{
				conferenceRoomData: function($stateParams, ConferenceRoomService){
					return ConferenceRoomService.get({id:$stateParams.id}).$promise.then(
						function( response ){
								return response;
						}
					);
				}
			}
		})



		.state('app.conferenceRoom.view.conference',{
			url: '/conference/:relationName',
			ncyBreadcrumb: {
				skip: true
			},
			data:{
				isTab:true
			},
			views: {
				"tabs": {
					templateUrl: 'app/conference/conference.list.html',
					controller: 'ConferenceListController'
				}
			}
		})
	
;
});