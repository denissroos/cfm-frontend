'use strict';

angular.module('angularDemoApp')
    .controller('ConferenceRoomViewController', function ($scope, $state, $stateParams, $translate, inform,
            ConferenceRoomService, conferenceRoomData,$mdDialog) {
	 	$scope.conferenceRoom = conferenceRoomData;

		if($state.current.data){
			$scope.isModal = $state.current.data.isModal;
		}

		$scope.deleteConferenceRoom = function(instance){
			return ConferenceRoomService.deleteInstance(instance).then(function(instance){
				$state.go('^.list');
				return instance;
			});
		};
		$scope.go = function(route){
			$state.go(route);
		};
		$scope.closeItemViewer = function () {
			$mdDialog.hide();
		};
	});
