'use strict';



angular.module('angularDemoApp')
    .controller('ParticipantEditController', function ($scope, $state, $q, $stateParams, ParticipantService, participantData, $translate, inform ) {
    	$scope.isEditForm = ($stateParams.id)?true:false;

		$scope.participant = participantData;


	    $scope.submit = function(frmController) {
			var deferred = $q.defer();
	    	var errorCallback = function(response){
					if (response.data.errors) {
		                angular.forEach(response.data.errors, function (error) {
							if(angular.element('#'+error.field).length) {
								frmController.setExternalValidation(error.field, undefined, error.message);
							} else {
								inform.add(error.message, {ttl: -1,'type': 'warning'});
							}
		                });
		            }
					deferred.reject(response);
		       };

	    	if($scope.isEditForm){
	    		ParticipantService.update($scope.participant, function(response) {	
	    			$translate('pages.participant.messages.update').then(function (msg) {
				    	inform.add(msg, {'type': 'success'});
					});
	            	deferred.resolve(response);
		        },errorCallback);
	    	}else{
    			ParticipantService.save($scope.participant,function(response) {
					
    				$translate('pages.participant.messages.create').then(function (msg) {
				    	inform.add(msg, {'type': 'success'});
					});
					$state.go('^.view', { id: response.id }, {location: 'replace'});
					deferred.resolve(response);
		        },errorCallback);
	    	}
	        return deferred.promise;
	    };
       
			   
			   
			   
			   
			   
			   
			   
	});
