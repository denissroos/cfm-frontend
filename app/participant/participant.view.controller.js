'use strict';

angular.module('angularDemoApp')
    .controller('ParticipantViewController', function ($scope, $state, $stateParams, $translate, inform,
            ParticipantService, participantData,$mdDialog) {
	 	$scope.participant = participantData;

		if($state.current.data){
			$scope.isModal = $state.current.data.isModal;
		}

		$scope.deleteParticipant = function(instance){
			return ParticipantService.deleteInstance(instance).then(function(instance){
				$state.go('^.list');
				return instance;
			});
		};
		$scope.go = function(route){
			$state.go(route);
		};
		$scope.closeItemViewer = function () {
			$mdDialog.hide();
		};
	});
