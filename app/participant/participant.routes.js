'use strict';

angular.module('angularDemoApp')
	.config(function ($stateProvider) {
$stateProvider
		.state('app.participant', {
		    url: '/participant',
			abstract: true,
		    template: '<div ui-view="page" class="fade-in-up"></div>'
		})
		.state('app.participant.list', {
			url: '/list?search',//TODO: search so that search is not an object in url
			views: {
				"page@app.participant": {
					templateUrl: 'app/participant/participant.list.html',
					controller: 'ParticipantListController'
				}
			}
		}).state('app.participant.create',{
			url: '/create',
			ncyBreadcrumb: {
				parent: 'app.participant.list'
			},
			views: {
				"page@app.participant": {
					templateUrl: 'app/participant/participant.form.html',
					controller: 'ParticipantEditController'
				}
			},
			resolve:{
				participantData: function($stateParams, ParticipantService) {
					return new ParticipantService();
				}
			}
		}).state('app.participant.view',{
			url: '/view/:id',
			ncyBreadcrumb: {
				parent: 'app.participant.list'
			},
			views: {
				"page@app.participant": {
					templateUrl: 'app/participant/participant.view.html',
					controller: 'ParticipantViewController'
				}
			},
			resolve:{
				participantData: function($stateParams, ParticipantService){
					return ParticipantService.get({id:$stateParams.id}).$promise.then(
						function( response ){
							return response;
						}
					);
				}
			}
		}).state('app.participant.view.edit',{
			url: '/edit',
			views: {
				"page@app.participant": {
					templateUrl: 'app/participant/participant.form.html',
					controller: 'ParticipantEditController',
				}
			},
			resolve:{
				participantData: function($stateParams, ParticipantService){
					return ParticipantService.get({id:$stateParams.id}).$promise.then(
						function( response ){
								return response;
						}
					);
				}
			}
		})


	.state('app.participant.view.conferenceModal',{
		url: '/modal/conference/:modalId',

		data:{
			isModal:true
		},
		onEnter: function($stateParams, $state, $mdDialog, $resource) {
			var modalId = $stateParams.modalId;
			$mdDialog.show({
				templateUrl: 'app/conference/conference.view.modal.html',
				resolve: {
					conferenceData: function($stateParams, ConferenceService){
						//TODO: Add parent ($stateParams.id) to query
						return ConferenceService.get({id:modalId}).$promise.then(
							function( response ){
								return response;
							}
						);
					}
				},
				controller: 'ConferenceViewController',

			}).then(function () {
				$state.go('^');
			});
		}

	}).state('app.participant.view.edit.conferenceSearchModal',{
		templateUrl: 'app/conference/conference.list.modal.html',
		controller: 'ConferenceListController'
	})
	

;
});